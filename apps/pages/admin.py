from django.contrib import admin
from .models import *


max_objects = 1

class ProfileAdmin(admin.ModelAdmin):
     def has_add_permission(self, request):
        if self.model.objects.count() >= max_objects:
            return False
        else:
            return True
     def has_delete_permission(self, request, obj = None):
            return False
admin.site.register(Navbar)
admin.site.register(Profile,ProfileAdmin)
admin.site.register(Gallery)
admin.site.register(Contact)
admin.site.register(Contact_detail, ProfileAdmin)
admin.site.register(Resume, ProfileAdmin)
admin.site.register(Project)

