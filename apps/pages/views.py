from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.http import HttpResponse


def navbar(request, slug):
    navbar = Navbar.objects.all().order_by("id")
    profile = Profile.objects.all()
    contact = Contact_detail.objects.all
    resume = Resume.objects.all
    gallery = Gallery.objects.all
    project = Project.objects.all
    for item in navbar:
        if request.path.startswith(item.get_absolute_url()):
            item.is_active = True
    if slug == "resume":
        return render(request, "resume.html", {"navbar": navbar, "resume": resume})
    if slug == "projects":
        return render(request, "projects.html", {"navbar": navbar, "project": project})
    if slug == "contact":
        if request.method == "POST":
            form = ContactForm(request.POST)
            if form.is_valid():
                form.save()
            return render(
                request,
                "contact.html",
                {"navbar": navbar, "contact": contact, "form": form},
            )
        else:
            form = ContactForm()
        return render(
            request,
            "contact.html",
            {"navbar": navbar, "contact": contact, "form": form},
        )
    return render(
        request, "home.html", {"navbar": navbar, "profile": profile, "gallery": gallery}
    )


def home(request):
    navbar = Navbar.objects.all().order_by("id")
    profile = Profile.objects.all()
    gallery = Gallery.objects.all

    return render(
        request, "home.html", {"navbar": navbar, "profile": profile, "gallery": gallery}
    )
