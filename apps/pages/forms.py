from django import forms
from .models import Contact
from phonenumber_field.formfields import PhoneNumberField

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'email', 'phone', 'subject', 'message']