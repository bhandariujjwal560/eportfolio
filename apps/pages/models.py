from django.db import models
from autoslug import AutoSlugField
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField


class Navbar(models.Model):
    title = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="title")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("navbar_detail", kwargs={"slug": self.slug})


class Profile(models.Model):
    profile_name = models.CharField(max_length=50)
    profile_pic = models.ImageField(upload_to="media/profile")
    profile_description = models.TextField()
    profile_quotes = models.CharField(max_length=200)

    def __str__(self):
        return self.profile_name


class Gallery(models.Model):
    title = models.CharField(max_length=50)
    photo = models.ImageField(upload_to="media/gallery")

    def __str__(self):
        return self.title


class Contact_detail(models.Model):
    address = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=254)
    site = models.URLField(max_length=200)


class Contact(models.Model):
    name = models.CharField(max_length=70)
    email = models.EmailField(max_length=254)
    phone = PhoneNumberField(blank=False)
    subject = models.CharField(max_length=50)
    message = models.TextField(max_length=200)

    def __str__(self):
        return self.name


class Resume(models.Model):
    resume_img = models.ImageField(upload_to="media/resume")

class Project(models.Model):
   title = models.CharField( max_length=50)
   description = models.TextField()
   photo = models.ImageField(upload_to='media/project')
    
   def __str__(self):
        return self.title